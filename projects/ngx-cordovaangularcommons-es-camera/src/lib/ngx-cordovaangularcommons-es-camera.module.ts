import { NgModule } from '@angular/core';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';
import { NgxAngularCommonsEsAppModule } from 'ngx-angularcommons-es-app';

import { NgxMaterialCommonsEsCoreModule } from 'ngx-materialcommons-es-core';
import { NgxMaterialCommonsEsAppModule } from 'ngx-materialcommons-es-app';
import { NgxMaterialCommonsEsMobileModule } from 'ngx-materialcommons-es-mobile';

import { CordovaCameraComponent } from './components/cordova-camera/cordova-camera.component';

@NgModule({
	imports: [
			NgxAngularCommonsEsCoreModule,
			NgxAngularCommonsEsAppModule,
			NgxMaterialCommonsEsCoreModule,
			NgxMaterialCommonsEsAppModule,
			NgxMaterialCommonsEsMobileModule
	],
	declarations: [ CordovaCameraComponent ],
	exports: [ CordovaCameraComponent ]
})
export class NgxCordovaAngularCommonsEsCameraModule {}
